import socket
import struct

class TerrariaDummyServer:
	def __init__(self, port, message):
		self.port = port
		self.message = message
		
	def listen_once(self):
		s = socket.socket()
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.bind(("0.0.0.0",self.port))
		s.listen()
		client, address = s.accept()
		#TODO: send the message to the client here
		client.close()
		s.shutdown(1)
		s.close()
		return address
			
