import requests
import json
import threading
import time

#message_received(nickname,text,gateway)

class Matterbridge(threading.Thread):
	def __init__(self, apiserver, message_received, token = None, autostart=True, daemon=True):
		threading.Thread.__init__(self,daemon=daemon)
		self.apiserver = apiserver
		self.message_received = message_received
		self.headers = {}
		self.retrycounter = 0
		if token:
			self.headers["Authorization"] = "Bearer "+token
		self.restart = True
		if autostart:
			self.start()
		
	def run(self):
		session = requests.Session()
		while self.restart:
			try:
				result = session.get(self.apiserver+"/api/stream", stream=True, headers=self.headers)
				print("Matterbridge stream connection established!")
				for line in result.iter_lines():
					try:
						msg = json.loads(line)
						# a bit of a strange convention for a message_received event, I know
						if msg["event"] == "":
							self.message_received(msg["username"], msg["text"], msg["gateway"])
					except Exception as e:
						print("Error while processing messages from matterbridge:",e)
			except Exception as e:
				print("Error while receiving messages from matterbridge:",e)
			self.retrycounter += 1
			print("Matterbridge stream connection closed, retrying in "+str(2*self.retrycounter)+" seconds")
			time.sleep(2*self.retrycounter)
	
	def send(self, nickname, text, gateway):
		#print("matterbridge.send()",nickname,text,gateway)
		try:
			requests.post(self.apiserver+"/api/message", json = {'text':text,'username':nickname,'gateway':gateway}, timeout=1, headers=self.headers)
		except Exception as e:
			print("Error while sending a message to matterbridge:",e)
		
