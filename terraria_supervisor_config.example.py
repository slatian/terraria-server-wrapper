# the command used to start the terraria server
terraria_server_command = "/path/to/terraria/server/terraria-server-1422/TerrariaServer.bin.x86_64 -config /path/to/terraria/server/terraria-server-1422/serverconfig.txt"
# the port you configured for your terraria server
terraria_server_port = 7777
# check every 10 seconds if there are still players on the server
autoshutdown_interval = 10
# shutdown the server if n checks fail
autoshoutdown_at_counter = 3
# allow cheating by entering commands into the chat
allow_ingame_cheats = True

# External chat support
# Connect to a matterbridge api if set to true
matterbridge_enabled = False
# Where the api server is located (do not put a "/" at the end)
matterbridge_api_server = "http://127.0.0.1:4242"
# The gateway to send chat and join/leave server up/down messages to
matterbridge_chat_gateway = "Testgateway"
# If you have configured your matterbridge to require an authorization token
# You can set it here (replace the None with the quoted token)
matterbridge_auth_token = None
