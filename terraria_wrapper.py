from subprocess import Popen, PIPE

class TerrariaWrapper:
	def __init__(self, stdin, stdout):
		self.on_system_message = None
		self.on_chat_message = None
		self.stdin = stdin
		self.stdout = stdout
		self.players = []
		self.started = False
		self.stopped = False
		
	def line_read(self, line):
		if line.startswith(": "):
			line = line[2:]
		if callable(self.on_system_message):
			self.on_system_message(self, line)
			
		if line.endswith(" has joined."):
			self.parsed_message("join", line[:-len(" has joined.")])
		elif line.endswith(" has left."):
			self.parsed_message("left", line[:-len(" has left.")])
		elif line.startswith("<"):
			split = line[1:].split("> ",1)
			if (len(split) == 2):
				self.parsed_message("chat", split[0], split[1])
		elif line.endswith(" is connecting..."):
			self.parsed_message("connecting", None, line[:-len(" is connecting...")])
		elif line == "Server started":
			self.parsed_message("started")
		else:
			self.parsed_message("message", None, line)
	
	def parsed_message(self, mtype, player = None, arg = None):
		if callable(self.on_parsed_message):
			self.on_parsed_message(self, mtype, player, arg)
		if mtype == "join":
			self.players.append(player)
		elif mtype == "left":
			self.players.remove(player)
		elif mtype == "chat":
			if callable(self.on_chat_message):
				self.on_chat_message(self, player, arg)
		elif mtype == "started":
			self.started = True
		elif mtype == "stopped":
			self.stopped = True
		
	def readloop(self):
		for line in self.stdout:
			self.line_read(line[:-1].decode("utf-8"))
		self.parsed_message("stopped")
	
	def command(self, cmd):
		self.stdin.write((cmd+"\n").encode("utf-8"))
		self.stdin.flush()
	
	def exit(self):
		self.command("exit")
		
	def say(self, something):
		for line in something.split("\n"):
			self.command("say "+line)
	

def run_server(command, on_parsed_message = None, on_chat_message = None, on_system_message = None, on_server_start = None):
	with Popen(command, shell=True, stdout=PIPE, stdin=PIPE) as proc:
		wrapper = TerrariaWrapper(proc.stdin, proc.stdout)
		wrapper.on_parsed_message = on_parsed_message
		wrapper.on_chat_message = on_chat_message
		wrapper.on_system_message = on_system_message
		if callable(on_server_start):
			on_server_start(wrapper)
		try:
			wrapper.readloop()
		except:
			wrapper.exit()
			raise
